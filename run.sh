#!/bin/sh -ex

#docker build --tag rekgrpth/pgdbf .
#docker push rekgrpth/pgdbf
docker pull rekgrpth/pgdbf
docker network create --attachable --opt com.docker.network.bridge.name=docker docker || echo $?
docker volume create pgdbf
docker stop pgdbf || echo $?
docker rm pgdbf || echo $?
docker run \
    --env GROUP_ID=$(id -g) \
    --env LANG=ru_RU.UTF-8 \
    --env TZ=Asia/Yekaterinburg \
    --env USER_ID=$(id -u) \
    --hostname pgdbf \
    --name pgdbf \
    --network docker \
    --rm \
    --volume pgdbf:/home \
    rekgrpth/pgdbf sh update.sh
