FROM rekgrpth/gost
ENV GROUP=pgdbf \
    USER=pgdbf
VOLUME "${HOME}"
RUN set -eux; \
    mkdir -p "${HOME}"; \
    addgroup -S "${GROUP}"; \
    adduser -D -S -h "${HOME}" -s /sbin/nologin -G "${GROUP}" "${USER}"; \
    apk add --no-cache --virtual .build-deps \
        autoconf \
        automake \
        gcc \
        git \
        make \
        musl-dev \
    ; \
    mkdir -p "${HOME}/src"; \
    cd "${HOME}/src"; \
    git clone --recursive https://github.com/RekGRpth/pgdbf.git; \
    cd "${HOME}/src/pgdbf"; \
    autoreconf -fi; \
    ./configure; \
    make -j"$(nproc)" install; \
    cd "${HOME}"; \
    apk add --no-cache --virtual .pgdbf-rundeps \
        coreutils \
        findutils \
        grep \
        musl-utils \
        postgresql-client \
        runit \
        sed \
        unzip \
        $(scanelf --needed --nobanner --format '%n#p' --recursive /usr/local | tr ',' '\n' | sort -u | while read -r lib; do test ! -e "/usr/local/lib/$lib" && echo "so:$lib"; done) \
    ; \
    find /usr/local/bin /usr/local/lib -type f -exec strip '{}' \;; \
    apk del --no-cache .build-deps; \
    find / -type f -name "*.a" -delete; \
    find / -type f -name "*.la" -delete; \
    rm -rf "${HOME}" /usr/share/doc /usr/share/man /usr/local/share/doc /usr/local/share/man; \
    rm -f /var/spool/cron/crontabs/root; \
    echo done
